package com.renseki.app.totandroid.ui.aboutme

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.renseki.app.totandroid.model.Me
import com.renseki.app.totandroid.repository.MeRepository

class AboutMeViewModel : ViewModel() {

    private lateinit var meRepository: MeRepository

    lateinit var me: LiveData<Me>

    private val meToPersist = MutableLiveData<Me>()
    val persistedMe = Transformations.switchMap(
        meToPersist
    ) { me ->
        meRepository.persist(me)
    }

    fun setMeRepository(meRepository: MeRepository) {
        this.meRepository = meRepository
        this.me = meRepository.load()
    }

    fun persist(me: Me) {
        meToPersist.value = me
    }
}